'use strict';

var UserInitializationController = require('../../controllers/user-initialization-controller');
var logicPuzzle = require('../../models/logic-puzzle');
var homeworkQuizzes = require('../../models/homework-quizzes');
var userHomeworkQuizzes = require('../../models/user-homework-quizzes');
var apiRequest = require('../../services/api-request');
var constant = require('../../mixin/constant');
