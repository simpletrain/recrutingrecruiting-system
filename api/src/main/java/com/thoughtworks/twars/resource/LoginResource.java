package com.thoughtworks.twars.resource;

import com.thoughtworks.twars.bean.User;
import com.thoughtworks.twars.mapper.UserMapper;

import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/login")
public class LoginResource extends Resource {

    private UserMapper userMapper;

    public LoginResource() {
        super();
        userMapper = session.getMapper(UserMapper.class);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createUser(User user) {

        User resultUser = userMapper.getUserByEmailAndPassWord(user);
        if (resultUser == null) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        Map<String, Object> map = new HashMap<>();
        Map<String, String> userInfo = new HashMap<>();

        map.put("id", resultUser.getId());
        userInfo.put("uri", "user/" + resultUser.getId());

        map.put("userInfo", userInfo);

        session.close();

        return Response.status(Response.Status.OK).entity(map).build();
    }
}
